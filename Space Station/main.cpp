//
//  main.cpp
//  Space Station
//
//  Created by Drink on 19/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include <iostream>

#include "position.hpp"
#include "space_station.hpp"



using namespace std;
using namespace space_station_class_activity;


int main(int argc, const char * argv[]) {

    cout << "=====Start=====" << endl;

    {
        shared_ptr<Robot> robot = make_shared<Robot>();
        cout << "position" << endl;
        robot->print_position();
        cout << endl;

        cout << "set position" << endl;
        robot->set_position(1.0f, 2.0f, 3.0f);
        robot->print_position();
        cout << endl;

        cout << "station report" << endl;
        robot->print_station_status();
        cout << endl;

        cout << "set station" << endl;
        shared_ptr<SpaceStation> station = make_shared<SpaceStation>();
        robot->set_station(station);

        cout << "station report" << endl;
        robot->print_station_status();
        cout << endl;

    }

    cout << "=====" << endl;

    {
        shared_ptr<SpaceStation> station = make_shared<SpaceStation>();
        station->add_robots(5);
        station->print_status();
        cout << endl;

        shared_ptr<SpaceStation> station2 = make_shared<SpaceStation>();
        *station2 = *station;

        cout << "first robot change position" << endl;
        shared_ptr<Robot> first_robot = station->get_robot(0);
        first_robot->set_position(2.0f, 3.0f, 4.0f);
        station2->get_robot(0)->set_position(5.0f, 6.0f, 7.0f);
        shared_ptr<SpaceStation> station3 = make_shared<SpaceStation>(*station2);
        station3->get_robot(0)->set_position(8.0f, 9.0f, 1.0f);

        cout << "Station 1" << endl;
        first_robot->print_station_status();

        cout << "Station 2" << endl;
        station2->print_status();
        
        cout << "Station 3" << endl;
        station3->print_status();
    }

    cout << "=====" << endl;

    {
        // This json sample file is attached to this project
        const string json_file = "/Users/drink/ITGT533/Space Station/Space Station/space_station.json";
        shared_ptr<SpaceStation> station = make_shared<SpaceStation>();
        station->load_from_json(json_file);
        station->print_status();
    }

    cout << "=====End=====" << endl;

}
