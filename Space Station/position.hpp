//
//  position.hpp
//  Space Station
//
//  Created by Drink on 19/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef position_hpp
#define position_hpp

#include <stdio.h>

namespace space_station_class_activity {

    class Position {
    public:
        Position();
        Position(float t_x, float t_y, float t_z);
        virtual ~Position();

        void print() const;

        float x;
        float y;
        float z;
    };

    class Positionable {
    public:

        Positionable();
        virtual ~Positionable();

        virtual void set_position(float t_x, float t_y, float t_z);

        virtual void set_position(Position pos);

        virtual void print_position() const;

        virtual Position get_position() const;

    protected:

        Position m_position;

    };

}
#endif /* position_hpp */
