//
//  space_station.hpp
//  Space Station
//
//  Created by Drink on 19/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef space_station_hpp
#define space_station_hpp

#include <stdio.h>
#include <vector>
#include <memory>
#include <string>
#include <fstream>
#include "position.hpp"

// This file can be found at
// https://github.com/nlohmann/json/blob/develop/single_include/nlohmann/json.hpp
#include "json.hpp"

namespace space_station_class_activity {

    class SpaceStation;
    class Robot;

    class SpaceStation: public Positionable, public std::enable_shared_from_this <SpaceStation> {
    public:
        SpaceStation();
        SpaceStation(const SpaceStation &obj);
        virtual ~SpaceStation();

        void print_status() const;
        void add_robot(std::shared_ptr<Robot> t_robot);
        void add_robots(unsigned int t_number_of_robots);
        std::shared_ptr<Robot> get_robot(size_t t_index) const;
        size_t total_robots() const;
        void remove_all_robots();
        void load_from_json(std::string filename);

        SpaceStation& operator=(const SpaceStation& other);

    protected:
        std::vector< std::shared_ptr<Robot> > m_robots;
    };

    class Robot: public Positionable, public std::enable_shared_from_this <Robot> {
    public:
        Robot();
        virtual ~Robot();

        void set_station(std::weak_ptr<SpaceStation> t_station);
        void print_station_status() const ;

    protected:
        std::weak_ptr<SpaceStation> m_station;
    };
}

#endif /* space_station_hpp */
