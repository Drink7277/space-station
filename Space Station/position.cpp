//
//  position.cpp
//  Space Station
//
//  Created by Drink on 19/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "position.hpp"

namespace space_station_class_activity {

    Position::Position(): Position(0.0f, 0.0f, 0.0f) {}
    Position::Position(float t_x, float t_y, float t_z): x(t_x), y(t_y), z(t_z) {}
    Position::~Position() {}

    void Position::print() const {
        printf("(%2.2f, %2.2f, %2.2f)\n", x, y, z);
    }

    Positionable::Positionable() {}
    Positionable::~Positionable() {}

    void Positionable::set_position(float t_x, float t_y, float t_z){
        m_position.x = t_x;
        m_position.y = t_y;
        m_position.z = t_z;
    }

    void Positionable::set_position(Position t_position){
        m_position = t_position;
    }

    void Positionable::print_position() const {
        m_position.print();
    }

    Position Positionable::get_position() const {
        return m_position;
    }
}
