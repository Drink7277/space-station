//
//  space_station.cpp
//  Space Station
//
//  Created by Drink on 19/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "space_station.hpp"

using json = nlohmann::json;

namespace space_station_class_activity {
 
    // SpaceStation
    SpaceStation::SpaceStation() {}

    SpaceStation::SpaceStation(const SpaceStation &obj) {
        // Due to objected is not finish created
        // robots in constructor cannot have pointer to the station
        // bad_weak_ptr will be throwed
        // *this = obj;
        
        // This version is not assign robot's station
        set_position(obj.get_position());
        for(size_t i = 0; i < obj.total_robots(); i++){
            std::shared_ptr<Robot> robot = std::make_shared<Robot>();
            robot->set_position(obj.get_robot(i)->get_position());
            m_robots.push_back(robot);
        }
    }

    SpaceStation::~SpaceStation() {}

    void SpaceStation::print_status() const {
        printf("Station: ");
        this->print_position();
        printf("There are %lu robot(s)\n", m_robots.size());
        for(int i = 0; i < m_robots.size(); i++){
            printf("%2d:", i + 1);
            m_robots[i]->print_position();
        }
    }

    void SpaceStation::add_robot(std::shared_ptr<Robot> t_robot){
        // if there is no input robot in vector
        if(std::find(m_robots.begin(), m_robots.end(), t_robot) == m_robots.end()) {
            m_robots.push_back(t_robot);

            std::weak_ptr<SpaceStation> w_this = shared_from_this();
            t_robot->set_station(w_this);
        }
    }

    void SpaceStation::add_robots(unsigned int t_number_of_robots){
        for(int i=0; i < t_number_of_robots; i++){
            std::shared_ptr<Robot> robot = std::make_shared<Robot>();
            add_robot(robot);
        }
    }

    std::shared_ptr<Robot> SpaceStation::get_robot(size_t t_index) const {
        return m_robots[t_index];
    }

    size_t SpaceStation::total_robots() const {
        return m_robots.size();
    }

    void SpaceStation::remove_all_robots(){
        m_robots.clear();
    }

    SpaceStation& SpaceStation::operator=(const SpaceStation& other){
        if (this == &other) { return *this; }
        this->set_position(other.get_position());
        this->remove_all_robots();

        for(size_t i = 0; i < other.total_robots(); i++){
            std::shared_ptr<Robot> robot = std::make_shared<Robot>();
            robot->set_position(other.get_robot(i)->get_position());
            this->add_robot(robot);
        }

        return *this;
    }

    void SpaceStation::load_from_json(std::string filename){
        std::ifstream file (filename);
        if (file.is_open())
        {
            json j = json::parse(file);

            // set station position
            auto pos = j["space_station"]["position"];
            float x = pos[0];
            float y = pos[1];
            float z = pos[2];
            this->set_position(x, y, z);

            // create and set robot position
            auto robots = j["space_station"]["robots"];
            for(auto i = 0; i < robots.size(); i++){
                auto robot = robots[i];
                auto pos = robot["position"];
                float x = pos[0];
                float y = pos[1];
                float z = pos[2];
                std::shared_ptr<Robot> bot = std::make_shared<Robot>();
                bot->set_position(x,y,z);
                this->add_robot(bot);
            }
            file.close();
        } else {
            throw "cannot read file";
        }
    }

    // Robot
    Robot::Robot() {}
    Robot::~Robot() {}

    void Robot::set_station(std::weak_ptr<SpaceStation> t_station){
        if(t_station.expired()){
            throw "errors set expired station";
        } else {
            m_station = t_station;
            m_station.lock()->add_robot(shared_from_this());
        }
    }

    void Robot::print_station_status() const {
        if(m_station.expired()){
            printf("I don't have station\n");
        } else {
            m_station.lock()->print_status();
        }
    }

}
